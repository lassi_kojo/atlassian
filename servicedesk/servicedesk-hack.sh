#!/bin/bash
# Author: Lassi Kojo <lassi.kojo@jmjping.fi>
# License: Simplified BSD License (http://opensource.org/licenses/BSD-3-Clause)
#
# Requirements: jar (fastJar seems to work)
#
# Usage: 
# ./servicedesk-hack.sh get
# Modify what you want
# ./servicedesk-hack.sh post
# Test if it works

VERSION=2.4.3
JIRA_USER=jira
EXTRACT_PATH=/root/service-desk
BACKUP_PATH=/root/jira-backup

function get() {
  mkdir -p $EXTRACT_PATH
  mkdir -p $BACKUP_PATH
  cp /var/atlassian/application-data/jira/plugins/installed-plugins/jira-servicedesk-$VERSION.jar $EXTRACT_PATH
  cd $EXTRACT_PATH
  echo "Extracting JAR file..."
  echo
  jar xf jira-servicedesk-$VERSION.jar
  mv jira-servicedesk-$VERSION.jar $BACKUP_PATH
}

function compress() {
  echo "Changing ownership back to ${JIRA_USER}..."
  echo
  chown -R $JIRA_USER:$JIRA_USER $EXTRACT_PATH
  cd $EXTRACT_PATH
  echo "Compressing back to JAR file..."
  echo
  jar cf jira-servicedesk-$VERSION.jar *
}

function move() {
  echo "Moving JAR file back to installation directory..."
  echo
  cd $EXTRACT_PATH
  mv jira-servicedesk-$VERSION.jar /var/atlassian/application-data/jira/plugins/installed-plugins/jira-servicedesk-$VERSION.jar
  echo "Changing ownership and permissions..."
  echo
  chown $JIRA_USER:$JIRA_USER /var/atlassian/application-data/jira/plugins/installed-plugins/jira-servicedesk-$VERSION.jar
  chmod 664 /var/atlassian/application-data/jira/plugins/installed-plugins/jira-servicedesk-$VERSION.jar
}

function restart() {
  echo "Restarting JIRA..."
  echo
  service jira stop && service jira start
}

function clean() {
  rm -rf $EXTRACT_PATH
}

function post() {
  compress
  move
  restart
  echo
  echo "Done!"
  echo
}

# call arguments verbatim:
$@
